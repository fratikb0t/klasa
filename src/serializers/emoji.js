const { Serializer } = require('klasa');

module.exports = class extends Serializer {

	async deserialize(data, piece, language) {
		const emoji = this.client.emojis.get(data);
		if (emoji) return emoji;
		throw language.get('RESOLVER_INVALID_EMOJI', piece.key);
	}

	serialize(value) {
		return (value || {}).id ? value.id : value;
	}

	stringify(value) {
		return (this.client.emojis.get(value) || { id: (value && value.id) || value }).id;
	}

};
